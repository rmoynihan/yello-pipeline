from distutils.command.build import build as _build
import setuptools
import logging
import subprocess


class build(_build):
    sub_commands = _build.sub_commands + [('CustomCommands', None)]

class CustomCommands(setuptools.Command):
    """Setuptools Command class to run arbitrary commands on Dataflow worker nodes."""

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def RunCustomCommand(self, command_list):
        logging.info(f'Running command: {command_list}')

        # Start process
        p = subprocess.Popen(
            command_list,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT
        )

        # Get status
        stdout_data, _ = p.communicate()
        logging.info(f'Command output: {stdout_data}')

        # Raise error if process fails
        if p.returncode != 0:
            raise RuntimeError(f'Command {command_list} failed: exist code: {p.returncode}')

    def run(self):
        for command in CUSTOM_COMMANDS:
            self.RunCustomCommand(command)


# List of commands to run
CUSTOM_COMMANDS = [
    ['python', '-m', 'nltk.downloader', 'punkt'],
    ['python', '-m', 'nltk.downloader', 'opinion_lexicon'],
    ['python', '-m', 'nltk.downloader', 'averaged_perceptron_tagger'],
]

# List of required packages
REQUIRED_PACKAGES = [
    'datetime',
    'nltk',
    'pandas',
    'textblob',
    'google-cloud-storage',
    'google-cloud-firestore',
    'scikit-learn==0.23.1',
]

setuptools.setup(
    name='pipeline-test',
    version='0.0.1',
    description='pipeline-testing setup',
    install_requires=REQUIRED_PACKAGES,
    packages=setuptools.find_packages(),
    cmdclass={
        'build': build,
        'CustomCommands': CustomCommands,
    }
)
