import apache_beam as beam
from apache_beam.options.pipeline_options import PipelineOptions, GoogleCloudOptions, SetupOptions, StandardOptions

import os
import re
import json
import pandas as pd
import pickle
import sys
import logging

# from nltk.corpus import stopwords, opinion_lexicon
# from textblob import TextBlob


# Set environmental variables
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = './yello-service-key.json'

# Set GCP project and topic
PROJECT = "yello-275819"
TOPIC = "projects/yello-275819/topics/newsriver"

##################
## Beam Classes ##
##################

class Tokenize(beam.DoFn):
    """
    Perform initial pre-processing of streamed JSON data


    """
    def setup(self):
        # Imports
        from nltk.stem import WordNetLemmatizer, PorterStemmer


        # Set constants
        self.WINDOW_SIZE = 3
        self.WORD_LIMIT = 1000
        self.PLACEHOLDER_CHAR = '_'
        self.stemmer = PorterStemmer()

    def process(self, element):
        # Imports
        from nltk import word_tokenize


        # Remove parts of the document surrounded by quotes
        element['text'] = re.sub(r'"+[^"]+"+', '', element['text'])

        # Lower case
        element['text'] = element['text'].lower()

        # Tokenize the document
        tokens = word_tokenize(element['text'])

        tokens = tokens[:self.WORD_LIMIT]

        # Remove non-word characters
        tokens = [re.sub(r'[^\w]', '', t) for t in tokens]

        # Set empty list elements to a placeholder char
        tokens = [self.PLACEHOLDER_CHAR if len(t) == 0 else t for t in tokens]

        # Stem tokens
        tokens = [self.stemmer.stem(t) for t in tokens]

        # Make all window combinations out of the tokens
        token_windows = [tokens[i:i+self.WINDOW_SIZE] for i in range(len(tokens))]

        # Remove any windows not equal to window size
        token_windows = [w for w in token_windows if len(w) == self.WINDOW_SIZE]

        # Yield each window separately, along with the article's ID
        element['token_windows'] = []
        for window in token_windows:
            element['token_windows'].append(window)

        yield element


class Features(beam.DoFn):
    """
    Creates and fits the required features for each set of tokens
    """
    def setup(self):
        # Imports
        from google.cloud import storage
        import pickle
        from nltk.stem import PorterStemmer
        import tempfile


        # Constants
        self.WINDOW_SIZE = 3
        self.TARGET_WORD_INDEX = int(self.WINDOW_SIZE/2)

        # Download files from GCP
        client = storage.Client()
        def get_file(filename):
            blob = client.get_bucket('yello-aiplatform').get_blob(f'lexicons/{filename}')
            blob = blob.download_as_string().decode('utf-8')
            blob = blob.split('\n')
            return blob

        # Load lexicons from text files
        self.bias_types = {}
        self.bias_types['assertives'] = [line.strip() for line in get_file('assertives_hooper1975.txt')]
        self.bias_types['factives'] = [line.strip() for line in get_file('factives_hooper1975.txt')]
        self.bias_types['hedges'] = [line.strip() for line in get_file('hedges_hyland2005.txt')]
        self.bias_types['implicatives'] = [line.strip() for line in get_file('implicatives_karttunen1971.txt')]
        self.bias_types['report_verbs'] = [line.strip() for line in get_file('report_verbs.txt')]  # Technically opposite of 'biased'
        self.bias_types['biased_lexicon'] = [line.strip() for line in get_file('bias_lexicon_lmadbl.txt')]
        self.bias_types['weak_subj'] = [line.strip() for line in get_file('weak_subj.txt')]
        self.bias_types['strong_subj'] = [line.strip() for line in get_file('strong_subj.txt')]
        self.bias_types['opinions'] = [line.strip() for line in get_file('opinions.txt')]

        # Stem each word in each lexicon
        stemmer = PorterStemmer()
        for b_type in self.bias_types:
            self.bias_types[b_type] = [stemmer.stem(w) for w in self.bias_types[b_type]]


        # Download and load the pickled features list
        filename = 'data_enc_onehot_features.sav'

        # Get file from GCP
        blob = client.get_bucket('yello-aiplatform').get_blob(f'features/{filename}')
        with tempfile.NamedTemporaryFile(delete=False) as file:
            blob.download_to_file(file)
        file.close()

        # Load file
        self.features = pickle.load(open(file.name, 'rb'))

    def process(self, element):
        # Imports
        import pandas as pd
        from nltk import pos_tag


        # Convert element to dataframe
        # df = pd.DataFrame(columns=['tokens'])

        # df.loc[-1] = element
        df = pd.DataFrame(element['token_windows'])
        df['tokens'] = df.values.tolist()
        df = df[['tokens']]

        # Separate out words
        for i in range(0, self.WINDOW_SIZE):
            df['word_' + str(i - self.TARGET_WORD_INDEX)] = df['tokens'].apply(lambda row: row[i])

        # POS tag each word
        pos_tags = df['tokens'].apply(pos_tag)
        for i in range(0, self.WINDOW_SIZE):
            df['pos_' + str(i - self.TARGET_WORD_INDEX)] = pos_tags.apply(lambda x: x[i][1])

        # Check for presence of biased words in tokens
        for i in range(0, self.WINDOW_SIZE):
            for b_type in self.bias_types:
                df[b_type + '_' + str(i - self.TARGET_WORD_INDEX)] = df['word_' + str(i - self.TARGET_WORD_INDEX)].isin(self.bias_types[b_type]).astype(int)

        # Get polarity and subjectivity of tokens
        try:
            df[['polarity', 'subjectivity']] = df.apply(lambda row: self.pol_and_subj(row), axis=1, result_type='expand')
        except ValueError:
            df.assign(polarity=0.0)
            df.assign(subjectivity=0.0)

        # OneHot encode words
        for i in range(0, self.WINDOW_SIZE):
            column = 'word_' + str(i - self.TARGET_WORD_INDEX)
            df = pd.get_dummies(df, prefix=column, columns=[column], drop_first=True)

        # OneHot encode POS tags
        for i in range(0, self.WINDOW_SIZE):
            column = 'pos_' + str(i - self.TARGET_WORD_INDEX)
            df = pd.get_dummies(df, prefix=column, columns=[column], drop_first=True)

        # Fit encodings to required features
        df = df.reindex(columns=self.features, fill_value=0)

        # Rearrange/drop/add some columns
        df.drop(['biased'], axis=1, inplace=True)
        # df.insert(loc=0, column='id', value=[element['id']])
        # df.insert(loc=0, column='tokens', value=[element['tokens']])

        # Yield dataframe as dict (list orient)
        # df = df.drop(df.columns[2:], axis=1)
        element['df'] = df
        # element = df.to_dict('list')
        yield element


    # Helper function
    def pol_and_subj(self, row):
        from textblob import TextBlob

        blob = TextBlob(' '.join(row['tokens']))

        if not blob.polarity:
            blob.polarity = 0
        if not blob.subjectivity:
            blob.subjectivity = 0

        return blob.polarity, blob.subjectivity


class Classify(beam.DoFn):
    """
    Downloads model and performs classification over elements
    """
    def setup(self):
        import pickle
        from google.cloud import storage
        import tempfile
        from sklearn.linear_model import LogisticRegression


        # Download and load the pickled features list
        filename = 'logistic_regression.sav'

        # Get file from GCP
        client = storage.Client()
        blob = client.get_bucket('yello-aiplatform').get_blob(f'models/bias_detection/{filename}')
        with tempfile.NamedTemporaryFile(delete=False) as file:
            blob.download_to_file(file)
        file.close()

        # Load file
        self.model = pickle.load(open(file.name, 'rb'))

    def process(self, element):
        # Imports
        import pandas as pd


        # Separate out non-prediction data
        df = element['df']
        # df = df.drop(df.columns[:2], axis=1)

        # Get prediction
        results = self.model.predict(df)

        # Give score for article
        score = 0
        element['bias'] = results.tolist().count(1) / len(element['token_windows'])
        element['biased_token_windows'] = [{'window':window} for window, biased in zip(element['token_windows'], results.tolist()) if biased == 1]

        # Remove unecessary features
        element.pop('df', None)
        element.pop('token_windows', None)

        yield element


class WriteToFirestore(beam.DoFn):
    """Write (or update) elements to Firestore collections"""
    def start_bundle(self):
        from google.cloud import firestore


        self.db = firestore.Client()

    def process(self, element):
        import datetime

        start_date = datetime.date(1900, 1, 1)
        today = datetime.date.today()
        collection_id = (today - start_date).days

        self.db.collection(f'{collection_id}').document(element['id']).set(element)



##########
## Main ##
##########

def main(argv=None):
    pipeline_options = {
        'runner': 'DataflowRunner',
        'job_name': 'pipeline-testing',
        'project': 'yello-275819',
        'region': 'us-central1',
        'temp_location': 'gs://yello-aiplatform/dataflow/temp/',
        'staging_location': 'gs://yello-aiplatform/dataflow/staging/',
        'setup_file': './setup.py',
        'streaming': True,
        'experiments': 'allow_non_updatable_job',
        'autoscaling_algorithm': 'THROUGHPUT_BASED',
        'max_num_workers': 4,
        'machine_type': 'n1-standard-1',
    }

    options = PipelineOptions.from_dictionary(pipeline_options)
    options.view_as(StandardOptions).streaming = True
    options.view_as(SetupOptions).save_main_session = True

    # options = PipelineOptions()
    p = beam.Pipeline(options=options)

    (p
        | "read from stream" >> beam.io.ReadFromPubSub(topic=TOPIC)
        | "flatmap JSON" >> beam.FlatMap(json.loads)
        | "pre-process & tokenize" >> beam.ParDo(Tokenize())
        | "featurize" >> beam.ParDo(Features())
        | "classify" >> beam.ParDo(Classify())
        # | "save" >> beam.io.WriteToText("gs://yello-aiplatform/dataflow/output/")
        | "firestore" >> beam.ParDo(WriteToFirestore())
    )

    result = p.run()
    result.wait_until_finish()


if __name__ == '__main__':
    logger = logging.getLogger().setLevel(logging.INFO)
    main()
